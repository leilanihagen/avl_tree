/* Leilani Hagen, 2018-07-07. */

#define TRUE 1
#define FALSE 0

struct node
{
  int data;
  struct node *left;
  struct node *right;
}
typedef struct node Node;

int search(Node *node, int num);
Node *add(Node *node, int num);
