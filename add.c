/* Leilani Hagen, 2018-07-07. */
#include <defs.h>

Node *add(Node *node, int num){
/* Recursive add. */

  if (search(node, num) == FALSE){
    return; // Datum already in tree.
  } // We now know that no existing node will have data equal to num...

  if (node == NULL){ // Base case - this is where the node creation happens.
    Node new_node = malloc(sizeof(Node));
    new_node->data = num;
    new_node->left = NULL;
    new_node->right = NULL;
    return new_node; // This is crucial for the next few steps.
  }

  if (num < node->data){ // Recurse left.
    return (node->left = add(node->left, num));
  }
  
  if (num > node->data){ // Recurse right.
    return (node->right = add(node->right, num));
  }
}
