/* Leilani Hagen, 2018-07-07. */
#include <defs.h>

int search(Node *node, int num) {
/* Preorder recursive search, effectively boolean. */

  if (node == NULL){ // Base case.
    return FALSE;
  }
    
  if (num == node->data){ // It makes sense to visit the node first...
    return TRUE;
  }
  else if (num < node->data){
    return(search(node->left));
  }
  else {
    return(search(node->right));
  }
}
